const { IgApiClient, LiveEntity } =  require('instagram-private-api');
const fs = require('fs');

const ig = new IgApiClient();


//var configFileRaw = fs.readFileSync("config.json");
//var config = JSON.parse(configFileRaw);

export async function login(t: any) {
    ig.state.generateDevice("dev");
    await ig.simulate.preLoginFlow();
    const user = await ig.account.login("u", "p");
    console.log(`login done, user: ${JSON.stringify(user)}`);
    Promise.resolve(user);
  }

//export async function getComments(){
    //console.log(`b:${bId} , x:${x}`)
    //x=await printComments(bId, x);
    //console.log(`mem used: ${process.memoryUsage().heapUsed / 1024 / 1024}`);
    //Promise.resolve(x);
 // }

 export async function createLive(w: number, h: number, m: string="An Instagram Live"){
    const { broadcast_id, upload_url } = await ig.live.create({
        previewWidth: 720,
        previewHeight: 1280,
        // not necessary, because it doesn't show up in the notification
        message: 'goo goo ga ga',
      });
      Promise.resolve({broadcast_id, upload_url});
}

export async function getUrlAndKey(bId: number, uUrl: string){
    const { stream_key, stream_url } = LiveEntity.getUrlAndKey({ bId, uUrl });
    Promise.resolve({stream_key, stream_url});
}

export async function startBroadcast(bId: number){
    const startInfo = await ig.live.start(bId);
    console.log(startInfo); //Log status
    Promise.resolve(status);
}