import * as React from 'react';

export interface ButtonProps { callback: (event: React.MouseEvent<HTMLButtonElement>)=>void, text: string; }

export default class Button extends React.Component<ButtonProps, {}>{

    constructor(props: any) {
        super(props);
    }

    public static defaultProps = {
        callback: ()=>{}
    };

    render(){
        return <button className="mainBut" onClick={this.props.callback ? this.props.callback:() => {}}>{this.props.text}</button>;
    }
}
