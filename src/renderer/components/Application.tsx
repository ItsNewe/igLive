import { hot } from 'react-hot-loader/root';
import * as React from 'react';

import Form from './Form';

const Application = () => (
    <div>
        <Form />
    </div>
);

export default hot(Application);
