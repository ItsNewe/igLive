import * as React from 'react';
import Button from './Button';

import {login} from '../actions/igLiveActions';

class Form extends React.Component{
    
    render(){
        return (
            <form action="#">
                <input type="text" placeholder="Username"></input>
                <input type="password" placeholder="Password"></input>
                <Button text="Connexion" callback={async ()=>{await login(this)}}/>
            </form>
        );
    }
}
export default Form;